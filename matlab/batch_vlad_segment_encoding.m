function batch_vlad_segment_encoding(kmeans_model, feat_dir, output_dir)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Encodes a bacth of C3D files using VLAD.
% Note: Requires VLFeat Toolbox.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fname = dir([feat_dir '/' '*.hdf5']);
for idx = 1:length(fname)
    c3d_feat_file = [feat_dir '/' fname(idx).name];
    output_filename = [output_dir '/' fname(idx).name];
    enc = vlad_segment_encoding(kmeans_model, c3d_feat_file, output_filename);
end
