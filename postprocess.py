import argparse
import glob
import os
import pickle as pkl
import subprocess

import h5py
import numpy as np
import pandas as pd

from utils import nms_detections

def postprocess_proposals(proposal_dir, df_gt, label_index, n_proposals,
                          weights_filename=None, det_thr=-0.8, alpha=1.0,
                          tiou_thr=0.3, proposal_threshold=None):
    # Get proposals from directory.
    filenames = glob.glob(os.path.join(proposal_dir, '*.csv'))
    # Concat proposals.
    proposals = pd.concat([pd.read_csv(f, sep=' ') for f in filenames], axis=0)
    # Define number of classes.
    cls_lst = np.arange(len(label_index))
    # Get videos to postprocess.
    video_lst = proposals['video-name'].unique()
    # Ratio to select an average number of proposals per video.
    select_ratio = (video_lst.shape[0] * n_proposals) / float(proposals.shape[0])

    # For each video, it creates a dataframe with postprocessed proposals.
    df_result = []
    for i, videoid in enumerate(video_lst):

        # Frame rate for this video (assume proposals frame index are computed
        # at same frame rate of ground truth file.)
        frame_rate = df_gt[df_gt['video-name'] == videoid]['frame-rate'].mean()
        video_duration = \
            df_gt[df_gt['video-name'] == videoid]['video-duration'].mean()

        # Filter by video.
        vid_idx = proposals['video-name'] == videoid
        this_proposals = proposals[vid_idx]
        # Order by descended proposal score.
        sort_idx = this_proposals['score'].argsort()[::-1]
        this_proposals = this_proposals.loc[sort_idx].reset_index(drop=True)

        # Filter by proposal score threshold.
        if proposal_threshold is not None:
            idx = this_proposals['score'] >= 0.7
            this_proposals = this_proposals[idx]

        # Average proposals per video.
        this_proposals = \
            this_proposals[:int(np.ceil(this_proposals.shape[0]*select_ratio))]

        # Get final predictions per class.
        for cls in cls_lst:
            cls = float(cls)
            # Discard proposals that do not satisfies detection threshold.
            det_idx = this_proposals['score_class_{}'.format(cls)] >= det_thr
            # If none proposal satisifies...
            if det_idx.sum() == 0:
                continue
            this_class_proposals = this_proposals[det_idx]

            # Apply nms (class dependent due to scores change between class.)
            pick = nms_detections(this_class_proposals[['f-init', 'f-end']].values,
                                  this_class_proposals['score_class_{}'.format(cls)].values.flatten(),
                                  tiou_thr)
            this_class_proposals = this_class_proposals.iloc[pick].reset_index(drop=True)

            nr_segments = this_class_proposals.shape[0]

            # Translate frames to time.
            t_init = this_class_proposals['f-init'].values / frame_rate
            t_end = this_class_proposals['f-end'].values / frame_rate
            # Map to real label id.
            labels = np.repeat(label_index[int(cls)], nr_segments).astype(int)
            # Computes score.
            scores = this_class_proposals['score_class_{}'.format(cls)].values.flatten()

            # Score refinement:
                # (a): combine proposal and classifier score.
                # (b): multiply score by a class specific window length prior.
            scores = scores + (alpha * this_class_proposals['score'])
            scores = scores * (t_end - t_init)
            if weights_filename:
                with open(weights_filename, 'rb') as fobj:
                    duration_weight = pkl.load(fobj)[int(cls)]
                for j in range(scores.shape[0]):
                    idx = np.abs(duration_weight['duration'] \
                        - (t_end[j] - t_init[j])).argmin()
                    scores[j] = scores[j] * duration_weight['weight'][idx]

            # Format proposals
            this_result = pd.DataFrame({'video-name': np.repeat(videoid,
                                                                nr_segments),
                                        't-init': t_init, 't-end': t_end,
                                        'cidx': labels, 'score': scores})
            df_result.append(this_result)

    # Concat detections for all videos.
    df_output = pd.concat(df_result, axis=0)
    return df_output

def main(proposal_dir, predictions_filename, gt_filename, label_index_filename,
         n_proposals, weights_filename=None, det_thr=-0.8, alpha=1.0,
         tiou_thr=0.3, proposal_threshold=None):
    # Read ground truth dataframe and label index.
    df_gt = pd.read_csv(gt_filename, sep=' ')
    label_index = pd.read_csv(label_index_filename, sep=' ',
                              header=None).loc[:, 0].tolist()

    # Postprocess proposals.
    df_output = postprocess_proposals(proposal_dir, df_gt, label_index,
                                      n_proposals,
                                      weights_filename=weights_filename,
                                      det_thr=det_thr, alpha=alpha,
                                      tiou_thr=tiou_thr,
                                      proposal_threshold=proposal_threshold)

    # Format according to THUMOS standard.
    col_order = ['video-name', 't-init', 't-end', 'cidx', 'score']
    df_output['score'] = df_output['score'].map(lambda x: '%2.5f' % x)
    df_output.to_csv(predictions_filename, sep='\t', index=False, header=False,
                     columns=col_order, float_format='%.1f')


if __name__ == '__main__':
    dsc = 'Format detection results following Thumos14 standard protocol.'
    p = argparse.ArgumentParser(description=dsc)
    p.add_argument('proposal_dir',
                   help='Dir containing the proposal detection csv files.')
    p.add_argument('predictions_filename',
                   help=('Path where the formatted detection '
                         'results will be dumped.'))
    p.add_argument('gt_filename',
                   help='Thumos14 CSV segments info file.')
    p.add_argument('label_index_filename',
                   help='class_index_detection file.')
    p.add_argument('n_proposals', type=int,
                   help='Average number of proposals to evaluate per video.')
    p.add_argument('--weights_filename', default=None,
                   help=('Pickle file containing weights for action '
                         'duration priors.'))
    p.add_argument('--det_thr', type=float, default=-0.8,
                   help=('Detection threshold. This may be expanded'
                         'to a threshold per class.'))
    p.add_argument('--alpha', type=float, default=1.0,
                   help='Weight to combine proposal and classifier score.')
    p.add_argument('--tiou_thr', type=float, default=0.3,
                   help='tiou threshold for class dependent nms.')
    p.add_argument('--proposal_threshold', type=float, default=None,
                   help='Threshold to be applied to the proposal score.')
    main(**vars(p.parse_args()))

